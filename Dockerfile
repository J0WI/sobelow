FROM golang:1.13 AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
RUN go build -o analyzer

FROM elixir:1.9.4-alpine

ENV MIX_HOME=/.mix

ARG SOBELOW_VERSION=0.8.0
ENV SOBELOW_VERSION=$SOBELOW_VERSION

RUN mix local.hex --force

RUN mix \
  archive.install --force \
  hex sobelow $SOBELOW_VERSION

COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
